Hi, My name is Kimberly Rose Lu.

I have had the privelege of working for one of the largest medical insurance companies in the US as a benefit analyst, configuring coverages according to customer specification as well as state regulations. While there I served as a stand-in lead, an SME and a QA.

My hobbies include gaming, cooking and eating, anime, powerlifting and football. I prefer RPGs the most as it is the least toxic. I also like watching food shows and trying out new recipes. For anime, I like comedies and isekai genres. And for my sports, I probably learn any sport I want quickly.